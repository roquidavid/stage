<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>WebGL Demo</title>
    <link rel="stylesheet" href="Css/design.css" type="text/css">
  </head>

  <body>
    <?php
    if((empty($_POST['info1']))||(empty($_POST['info2']))||(empty($_POST['info3']))){
      header('Location: index.html');
    }
    ?>
    <div class="display">
      <div class="choice">
        <span><button type="button" name="button1">1</button></span>
        <span><button type="button" name="button2">2</button></span>
        <span><button type="button" name="button3">3</button></span>
        <span><button type="button" name="button4">4</button></span>
      </div>
    </div>
    <canvas id="glcanvas" width="1280" height="720"></canvas>
  </body>

  <script src="scripts/matrix.js"></script>
  <script type="text/javascript"> var cubeSize = <?php echo $_POST['info1'];?></script>
  <script src="scripts/cube.js"></script>
</html>
