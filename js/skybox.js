function createSkybox(scene) {
  // Création d'une material
	 var sMaterial = new BABYLON.StandardMaterial("skyboxMaterial", scene);
	 sMaterial.backFaceCulling = false;
	 sMaterial.reflectionTexture = new BABYLON.CubeTexture("images/skybox/skybox", scene);
	 sMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;

	 // Création d'un cube avec la material adaptée
	 var skybox = BABYLON.Mesh.CreateBox("skybox", 250, scene);
	 skybox.material = sMaterial;
}
