const app = new WHS.App([
  new WHS.app.ElementModule(),
  new WHS.app.SceneModule(),
  new WHS.app.CameraModule({
    position: new THREE.Vector3(0, 0, 50)
  }),
  new WHS.app.RenderingModule({bgColor: 0x162129}),
  new WHS.app.ResizeModule()
]);

const app = new WHS.App(
  new WHS.BasicAppPreset() // setup for :
  // ElementModule
  // SceneModule
  // CameraModule
  // RenderingModule
);

// const app = ...

const sphere = new WHS.Sphere({ // Create sphere component.
  geometry: {
    radius: 3,
    widthSegments: 32,
    heightSegments: 32
  },

  material: new THREE.MeshBasicMaterial({
    color: 0xF2F2F2
  }),

  position: [0, 10, 0]
});

sphere.addTo(app); // Add sphere to world.


// const app = ...
// const sphere = ...

new WHS.Plane({
  geometry: {
    width: 100,
    height: 100
  },

  material: new THREE.MeshBasicMaterial({
    color: 0x447F8B
  }),

  rotation: {
    x: - Math.PI / 2
  }
}).addTo(app);

app.start(); // Start animations and physics simulation.
